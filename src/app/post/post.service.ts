import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable,throwError} from 'rxjs';
import {map,catchError} from 'rxjs/operators';
import {Post} from './interfaces/post';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostService {
   
  private apiURL = "https://jsonplaceholder.typicode.com";

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient, private firestore: AngularFirestore) { }

  documentToDomainObject = _ => {
    const object = _.payload.doc.data();
    object.id = _.payload.doc.id;
    return object;
  }
  create(post) {
    return new Promise<any>((resolve, reject) =>{
      this.firestore
          .collection("posts")
          .add(post)
          .then(res => {}, err => reject(err));
  });

  }

  getAll(): Observable<any[]> {
    return this.firestore.collection("posts").snapshotChanges()
    .pipe(map(actions => actions.map(this.documentToDomainObject)));
  }

  find(id): Observable<Post> {
    return this.httpClient.get<Post>(this.apiURL + '/posts/' + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  update(id, post=""): Observable<Post> {
     return this.httpClient.put<Post>(this.apiURL + '/posts/' + id, JSON.stringify(post), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updateCoffeeOrder(data) {
    return this.firestore
        .collection("posts")
        .doc(data.payload.doc.id)
        .set({ completed: true }, { merge: true });
 }
 
 delete(id: string): Promise<void> {
    return this.firestore.collection("posts").doc(id).delete();
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
 }
}
