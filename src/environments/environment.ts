// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyB_72VoxUgQdyGuY_PbqRfy_CDZg9hVf8I",
    authDomain: "my-assignment-7e214.firebaseapp.com",
    projectId: "my-assignment-7e214",
    storageBucket: "my-assignment-7e214.appspot.com",
    messagingSenderId: "952258146680",
    appId: "1:952258146680:web:318d15e7957a215943d88d",
    measurementId: "G-9MX0RHG3BC"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
